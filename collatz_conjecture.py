import sys

def collatz_conjecture(n):
    print (n)
    if n == 1:
        return
    elif n % 2 == 0:
        collatz_conjecture(n/2)
    else: #n is odd
        collatz_conjecture(3*n+1)

def main():
  print "**** Collatz Conjecture ****"
  try:
      n = int(raw_input('Enter an integer: '))
      if n < 1:
          raise ValueError
      collatz_conjecture(n)
  except ValueError:
      print "Invalid input. Must be integer > 0"

if __name__ == "__main__":
    main()
